/*
 * Copyright (C) 2019, The authors of the 5-elements-in-play project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.androb.five_elements_in_play;

import net.androb.five_elements_in_play.simulation_objects.CollisionIndicator;

import java.awt.Point;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Simulation {

	private final List<SimulationObject> list;
	private final Set<PositionedObject>[][] posGrid;
	private final CollidableObject[][] grid;
	private MainProcessing sketch;

	public Simulation(final MainProcessing sketch) {

		this.sketch = sketch;
		this.list = new LinkedList<>();
		this.posGrid = new Set[sketch.width][sketch.height];
		for (int gwi = 0; gwi < posGrid.length; gwi++) {
			for (int ghi = 0; ghi < posGrid.length; ghi++) {
				posGrid[gwi][ghi] = new HashSet<>();
			}
		}
		this.grid = new CollidableObject[sketch.width][sketch.height];
	}

	private void addToGrid(final PositionedObject posObj) {

		final Point position = posObj.getPosition();
		if (position == null || !isInsideSketch(position)) {
			// add to the grid later, when the position is set
		} else {
			posGrid[position.x][position.y].add(posObj);
			if (posObj instanceof CollidableObject) {
				final CollidableObject colObj = (CollidableObject) posObj;
				if (grid[position.x][position.y] == null) {
					grid[position.x][position.y] = colObj;

					// check neighbours for (near-)collision
					final boolean left = position.x > 0;
					final boolean right = position.x + 1 < getSketch().width;
					final boolean up = position.y > 0;
					final boolean down = position.y + 1 < getSketch().height;
					if (left && up   && grid[position.x - 1][position.y - 1] != null) {
						touching(grid[position.x - 1][position.y - 1], colObj);
					}
					if (left         && grid[position.x - 1][position.y] != null) {
						touching(grid[position.x - 1][position.y], colObj);
					}
					if (left && down && grid[position.x - 1][position.y + 1] != null) {
						touching(grid[position.x - 1][position.y + 1], colObj);
					}
					if (right && up   && grid[position.x + 1][position.y - 1] != null) {
						touching(grid[position.x + 1][position.y - 1], colObj);
					}
					if (right         && grid[position.x + 1][position.y] != null) {
						touching(grid[position.x + 1][position.y], colObj);
					}
					if (right && down && grid[position.x + 1][position.y + 1] != null) {
						touching(grid[position.x + 1][position.y + 1], colObj);
					}
					if (         up   && grid[position.x][position.y - 1] != null) {
						touching(grid[position.x][position.y - 1], colObj);
					}
					if (         down && grid[position.x ][position.y + 1] != null) {
						touching(grid[position.x][position.y + 1], colObj);
					}
				} else {
					collision(position.x, position.y, grid[position.x][position.y], colObj);
				}
			}
		}
	}

	public void add(final SimulationObject simObj) {

		simObj.setSketch(sketch);
		if (simObj instanceof PositionedObject) {
			addToGrid((PositionedObject) simObj);
		}
		list.add(simObj);
	}

	private void collision(final int x, final int y, final CollidableObject objPresent, final CollidableObject objArriving) {

		add(new CollisionIndicator(x, y, objPresent, objArriving));
		remove(objArriving); // TODO rather combine with previous
	}

	private void touching(final CollidableObject objPresent, final CollidableObject objArriving) {

		collision(objArriving.getPosition().x, objArriving.getPosition().y, objPresent, objArriving);
	}

	public void remove(final SimulationObject simObj) {

		if (simObj instanceof PositionedObject) {
			final Point position = ((PositionedObject) simObj).getPosition();
			posGrid[position.x][position.y].remove(simObj);
			if (simObj instanceof CollidableObject) {
				grid[position.x][position.y] = null;
			}
		}
		list.remove(simObj);
	}

	public List<SimulationObject> getList() {
		return list;
	}

	private boolean isInsideSketch(final Point pos) {

		boolean inside = true;
		if (pos.x < 0) {
			inside = false;
		} else if (pos.x >= sketch.width) {
			inside = false;
		} else if (pos.y < 0) {
			inside = false;
		} else if (pos.y >= sketch.height) {
			inside = false;
		}

		return inside;
	}

	public void move(final PositionedObject obj, final Point posOld, final Point posNew) {

		if (posOld == null) {
			if (posNew == null) {
				return;
			}
		} else if (posOld.equals(posNew)) {
			return;
		}

		if (posOld != null && isInsideSketch(posOld)) {
			posGrid[posOld.x][posOld.y].remove(obj);
			if (obj instanceof CollidableObject) {
				grid[posOld.x][posOld.y] = null;
			}
		}
		addToGrid(obj);
	}

	public MainProcessing getSketch() {
		return sketch;
	}

	public CollidableObject[][] getGrid() {
		return grid;
	}
}
