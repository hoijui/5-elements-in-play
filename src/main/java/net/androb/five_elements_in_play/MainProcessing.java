/*
 * Copyright (C) 2019, The authors of the 5-elements-in-play project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.androb.five_elements_in_play;

import net.androb.five_elements_in_play.simulation_objects.EarthElement;
import net.androb.five_elements_in_play.simulation_objects.Element;
import net.androb.five_elements_in_play.simulation_objects.WaterElement;
import processing.core.PApplet;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("jol")
public class MainProcessing extends PApplet {

	private Simulation simulation;
	private boolean drawNormal = true;

	@Override
	public void settings() {
		size(50, 50);
	}

	@Override
	public void setup() {

		simulation = new Simulation(this);
		simulation.add(new EarthElement());
		colorMode(RGB, 255);
	}

	@Override
	public void draw() {

		background(0);
		if (drawNormal) {
			noStroke();
			// we make this copy to prevent ConcurrentModificationException
			final List<SimulationObject> simObjectsCopy = new LinkedList<>(getSimulation().getList());
			for (final SimulationObject obj : simObjectsCopy) {
				obj.advance();
				obj.draw();
			}
		} else {
			// draw the collidable-grid
			stroke(255, 255, 255);
			final CollidableObject[][] grid = simulation.getGrid();
			for (int gwi = 0; gwi < grid.length; gwi++) {
				for (int ghi = 0; ghi < grid.length; ghi++) {
					if (grid[gwi][ghi] != null) {
						rect(gwi, ghi, 1, 1);
					}
				}
			}
		}
	}

	@Override
	public void mousePressed() {

		if (mouseButton == CENTER) {
			drawNormal = !drawNormal;
		}
	}

	@Override
	public void mouseReleased() {

		if (mouseButton == CENTER) {
			drawNormal = !drawNormal;
		}
	}

	@Override
	public void mouseClicked() {

		if (mouseButton == CENTER) {
			return;
		}
		final Element element;
		if (mouseButton == LEFT) {
			element = new EarthElement();
		} else {
			element = new WaterElement();
		}
		simulation.add(element);
		element.setPosition(new Point(mouseX, mouseY));
	}

	public Simulation getSimulation() {
		return simulation;
	}
}
