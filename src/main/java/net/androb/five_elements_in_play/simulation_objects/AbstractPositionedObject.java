/*
 * Copyright (C) 2019, The authors of the 5-elements-in-play project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.androb.five_elements_in_play.simulation_objects;

import net.androb.five_elements_in_play.MainProcessing;
import net.androb.five_elements_in_play.PositionedObject;

import java.awt.Point;

public abstract class AbstractPositionedObject implements PositionedObject {

	private MainProcessing sketch;
	private Point position;

	public AbstractPositionedObject() {

		this.sketch = null;
		this.position = null;
	}

	protected MainProcessing getSketch() {
		return sketch;
	}

	@Override
	public void setSketch(final MainProcessing sketch) {
		this.sketch = sketch;
	}

	@Override
	public void setPosition(final Point position) {

//		this.position.setLocation(position);
		final Point oldPos = this.position;
		this.position = position;
		if (sketch != null && oldPos != null) {
			sketch.getSimulation().move(this, oldPos, this.position);
		}
	}

	@Override
	public Point getPosition() {
		return position;
	}
}
