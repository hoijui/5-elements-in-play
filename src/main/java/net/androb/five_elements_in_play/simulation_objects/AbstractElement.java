/*
 * Copyright (C) 2019, The authors of the 5-elements-in-play project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.androb.five_elements_in_play.simulation_objects;

import net.androb.five_elements_in_play.MainProcessing;

import java.awt.Color;
import java.awt.Point;
import java.util.Random;

public abstract class AbstractElement extends AbstractPositionedObject implements Element {

	/**
	 * Trajectory and inertia combined, as in:
	 * Where to and how fast are we going.
	 */
	private Point direction;
	private final Random random;
	private static final Point NO_DIRECTION = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

	public AbstractElement() {

		this.random = new Random();
	}

	protected Random getRandom() {
		return random;
	}

	protected Point createRandomSmallVector() {
		return new Point(
				getRandom().nextInt(7) - 3,
				getRandom().nextInt(7) - 3);
	}

	public abstract Color getColor();

	public Point createRandomPosition() {
		return new Point(random.nextInt(getSketch().width), random.nextInt(getSketch().height));
	}

	@Override
	public void setSketch(final MainProcessing sketch) {
		super.setSketch(sketch);

		setPosition(createRandomPosition());
		if (getRandom().nextBoolean()) {
			this.direction = createRandomSmallVector();
		} else {
			this.direction = NO_DIRECTION;
		}
	}

	public void moveAndBounceOffEdges(final Point nextPos) {

		if (nextPos.x < 0) {
			nextPos.x *= -1;
			direction.x *= -1;
		} else if (nextPos.x >= getSketch().width) {
			nextPos.x = (getSketch().width * 2) - nextPos.x;
			direction.x *= -1;
		}
		if (nextPos.y < 0) {
			nextPos.y *= -1;
			direction.y *= -1;
		} else if (nextPos.y >= getSketch().height) {
			nextPos.y = (getSketch().height * 2) - nextPos.y;
			direction.y *= -1;
		}
		setPosition(nextPos);
	}

	@Override
	public void advance() {
		// TODO Movin', doin' it ya' know!?
		final Point nextPos;
		if (direction == NO_DIRECTION) {
			// random small movement
			nextPos = createRandomSmallVector();
		} else {
			nextPos = direction.getLocation();
		}
		nextPos.translate(getPosition().x, getPosition().y);
		moveAndBounceOffEdges(nextPos);
	}

	@Override
	public void draw() {

		if (getPosition() != null) {
			int myColor = getSketch().color(getColor().getRed(), getColor().getGreen(), getColor().getBlue());
			getSketch().fill(myColor);
			// One pixel is very small
			//pApplet.rect(position.x, position.y, 1, 1);
			// .. so we make it big (6 x 6 pixels)
			getSketch().rect(getPosition().x - 3, getPosition().y - 3, 6, 6);
		}
	}
}
