/*
 * Copyright (C) 2019, The authors of the 5-elements-in-play project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.androb.five_elements_in_play.simulation_objects;

import net.androb.five_elements_in_play.CollidableObject;

import java.awt.Point;

public class CollisionIndicator extends AbstractPositionedObject {

	private final CollidableObject obj1;
	private final CollidableObject obj2;
	private int age;

	public CollisionIndicator(final int x, final int y, final CollidableObject obj1, final CollidableObject obj2) {

//		System.err.printf("CollisionIndicator at: (%d, %d)\n", x, y);
		this.obj1 = obj1;
		this.obj2 = obj2;
		setPosition(new Point(x, y));
		this.age = 0;
	}


	@Override
	public void advance() {

		age++;
		if (age >= 40) {
			getSketch().getSimulation().remove(this);
		}
	}

	@Override
	public void draw() {

		if (getPosition() != null) {
			int myColor = getSketch().color(255, 0, 0);
			getSketch().fill(myColor);
			getSketch().ellipse(getPosition().x - 3, getPosition().y - 3, 6, 6);
		}
	}
}
