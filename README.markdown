     _____      ______  _                                _
    | ____|    |  ____|| |                              | |
    | |__      | |__   | |  ___  _ __ ___    ___  _ __  | |_  ___
    |___ \     |  __|  | | / _ \| '_ ` _ \  / _ \| '_ \ | __|/ __|
     ___) |    | |____ | ||  __/| | | | | ||  __/| | | || |_ \__ \
    |____/     |______||_| \___||_| |_| |_| \___||_| |_| \__||___/

                 _             _____   _
                (_)           |  __ \ | |
                 _  _ __      | |__) || |  __ _  _   _
                | || '_ \     |  ___/ | | / _` || | | |
                | || | | |    | |     | || (_| || |_| |
                |_||_| |_|    |_|     |_| \__,_| \__, |
                                                  __/ |
                                                 |___/

<!--
    The title was created with:
    ```bash
    figlet -k -w 160 -f big "5    Elements"
    figlet -k -w 160 -f big "            in    Play"
    ```
-->

_5 Elements in Play_ is a little visual simulation of 5 types of particles roaming around
in a 2D World, and interacting with each other:

* Earth <img src="src/main/resources/icons/earth.svg" alt="earth-icon" width="128"/>
* Fire <img src="src/main/resources/icons/fire.svg" alt="fire-icon" width="128"/>
* Wind <img src="src/main/resources/icons/wind.svg" alt="wind-icon" width="128"/>
* Water <img src="src/main/resources/icons/water.svg" alt="water-icon" width="128"/>
* Space <img src="src/main/resources/icons/space.svg" alt="space-icon" width="128"/>

It is written in Java 8+, and uses the [Processing](https://processing.org/) engine.
The source code is published under the GPL v3,
and can be found [on gitlab](https://gitlab.com/hoijui/5-elements-in-play).
The build system in use is Maven.

## How to use

### Compile

    mvn package

### Run

    mvn exec:java

## Thanks

Thanks go to nature for the initial idea.

